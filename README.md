# react-todo

I made a simple todo app in React with a random todo api 

## Instructions :

- clone the repository
- $ cd react-todo
- $ npm install
- $ npm start

Hosted on https://react-todo-api.netlify.com