import React, { Component } from "react";
import Tasks from "./components/tasks";
import Form from "./components/form";
import Footer from "./components/footer";
import FilterTasks from "./components/filterTasks";
import "./App.css";

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      tasks: [],
      filterTasks: [],
    };
  }
  componentDidMount() {
    fetch("https://jsonplaceholder.typicode.com/todos")
      .then(res => res.json())
      .then(data => {
        this.setState({ tasks: data });
      })
      .catch(console.log);
  }
  deleteTask = id => {
    const tasks = this.state.tasks.filter(task => {
      return task.id !== id;
    });
    const filterTasks = this.state.filterTasks.filter(filterTask => {
      return filterTask.id !== id;
    });
    this.setState({
      tasks,
      filterTasks,
    });
  };
  addTask = task => {
    task.id = Math.random();
    let tasks = [...this.state.tasks, task];
    this.setState({
      tasks,
    });
  };
  todoTasks = () => {
    const filterTasks = this.state.tasks.filter(task => {
      return task.completed === false;
    });
      const tasks = this.state.tasks;
    if (this.refs.todo && this.refs.todo.checked) {
      this.setState({
        filterTasks,
      });
    }
 else {
      this.setState({
        filterTasks: tasks,
      });
    }
  };
  doneTasks = () => {
    const filterTasks = this.state.tasks.filter(task => {
      return task.completed === true;
    });
    const tasks = this.state.tasks;
    if (this.refs.done && this.refs.done.checked) {
      this.setState({
        filterTasks,
      });
    } else {
      this.setState({
        filterTasks: tasks,
      });
    }
  };

  render() {
    return (
      <div className="App-header">
        <h1>Todo's</h1>
        <Form addTask={this.addTask} />
        <div>
          <p>
            <label>
              <input
                type="checkbox"
                ref="done"
                onChange={() => {
                  this.doneTasks();
                }}
              />
              <span>Done tasks</span>
            </label>
          </p>
          <p>
            <label>
              <input
                type="checkbox"
                ref="todo"
                onChange={() => {
                  this.todoTasks();
                }}
              />
              <span>Todo tasks</span>
            </label>
          </p>
        </div>

        <br />
        <FilterTasks
          filterTasks={this.state.filterTasks}
          deleteTask={this.deleteTask}
        />
        <Tasks tasks={this.state.tasks} deleteTask={this.deleteTask} />
        <Footer />
      </div>
    );
  }
}

export default App;
