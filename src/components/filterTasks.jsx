import React from "react";

const FilterTasks = ({ deleteTask, filterTasks }) => {
  return (
    <div>
      {filterTasks.map((filterTask, index) => (
        <div key={index} className="collection">
          <span>{filterTask.title} </span>{" "}
          <button
            className="waves-effect waves-light red btn-small"
            onClick={() => {
              deleteTask(filterTask.id);
            }}
          >
            X
          </button>
        </div>
      ))}
    </div>
  );
};

export default FilterTasks;
