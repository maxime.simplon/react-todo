import React from "react";

const Tasks = ({ tasks, deleteTask }) => {
  return (
    <div>
      {tasks.map((task, index) => (
        <div key={index} className="collection">
          <span>{task.title} </span>{" "}
          <button
            className="waves-effect waves-light red btn-small"
            onClick={() => {
              deleteTask(task.id);
            }}
          >
            X
          </button>
        </div>
      ))}
    </div>
  );
};

export default Tasks;
