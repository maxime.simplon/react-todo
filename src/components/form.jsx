import React, { Component } from "react";

class Form extends Component {
  state = {
    title: ""
  };
  handleChange = e => {
    this.setState({
      title: e.target.value
    });
  };
  handleSubmit = e => {
    e.preventDefault()
    this.props.addTask(this.state)
    this.setState({
        title : ''
    })
  };
  render() {
    return (
      <form onSubmit={this.handleSubmit}>
        <input onChange={this.handleChange} value={this.state.title} placeholder="enter task" />
        <button
          className="btn waves-effect waves-light"
          type="submit"
          name="action"
        >
          Add
        </button>
      </form>
    );
  }
}

export default Form;
